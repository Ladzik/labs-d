/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabD.zoo;

import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author st
 */
public class Monkey implements pk.labs.LabD.contracts.Animal {

    private String name;
    private String status;
    private List <PropertyChangeListener> propertyList = new LinkedList<PropertyChangeListener>();
    
    
    @Override
    public String getSpecies() {
        return "Aaaaa";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyList.add(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyList.remove(listener);
    }
}
